<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddShippingMethodColumnInOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\Schema::table('orders', function (Blueprint $table) {
            $table->string('shipping_method')->default('deal of seal');
            $table->unsignedBigInteger('shipment_policy')->nullable();
            $table->string('shipment_label_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('shipping_method');
            $table->dropColumn('shipment_policy');
            $table->dropColumn('shipment_lable_url');
        });
    }
}
