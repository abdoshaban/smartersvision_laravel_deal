<?php

return array (
  'title' => 'مثبت Laravel',
  'next' => 'الخطوة التالية',
  'back' => 'السابق',
  'finish' => 'تثبيت',
  'forms' => 
  array (
    'errorTitle' => 'وقعت الأخطاء التالية:',
  ),
  'welcome' => 
  array (
    'templateTitle' => 'أهلا بك',
    'title' => 'مثبت Laravel',
    'message' => 'معالج التثبيت والإعداد السهل.',
    'next' => 'تحقق من المتطلبات',
  ),
  'requirements' => 
  array (
    'templateTitle' => 'الخطوة 1 | متطلبات الخادم',
    'title' => 'متطلبات الخادم',
    'next' => 'تحقق من الأذونات',
  ),
  'permissions' => 
  array (
    'templateTitle' => 'الخطوة 2 | أذونات',
    'title' => 'أذونات',
    'next' => 'تكوين البيئة',
  ),
  'environment' => 
  array (
    'menu' => 
    array (
      'templateTitle' => 'الخطوة 3 | إعدادات البيئة',
      'title' => 'إعدادات البيئة',
      'desc' => 'يرجى تحديد الطريقة التي تريد بها تهيئة ملف التطبيقات <code> .env </code>.',
      'wizard-button' => 'إعداد معالج النماذج',
      'classic-button' => 'محرر نص كلاسيكي',
    ),
    'wizard' => 
    array (
      'templateTitle' => 'الخطوة 3 | إعدادات البيئة | معالج موجه',
      'title' => 'معالج <code> .env </code> الإرشادي',
      'tabs' => 
      array (
        'environment' => 'بيئة',
        'database' => 'قاعدة البيانات',
        'application' => 'تطبيق',
      ),
      'form' => 
      array (
        'name_required' => 'مطلوب اسم البيئة.',
        'app_name_label' => 'اسم التطبيق',
        'app_name_placeholder' => 'اسم التطبيق',
        'app_environment_label' => 'بيئة التطبيق',
        'app_environment_label_local' => 'محلي',
        'app_environment_label_developement' => 'تطوير',
        'app_environment_label_qa' => 'ق',
        'app_environment_label_production' => 'إنتاج',
        'app_environment_label_other' => 'آخر',
        'app_environment_placeholder_other' => 'أدخل بيئتك ...',
        'app_debug_label' => 'تصحيح التطبيق',
        'app_debug_label_true' => 'صحيح',
        'app_debug_label_false' => 'خاطئة',
        'app_log_level_label' => 'مستوى سجل التطبيق',
        'app_log_level_label_debug' => 'التصحيح',
        'app_log_level_label_info' => 'معلومات',
        'app_log_level_label_notice' => 'تنويه',
        'app_log_level_label_warning' => 'تحذير',
        'app_log_level_label_error' => 'خطأ',
        'app_log_level_label_critical' => 'حرج',
        'app_log_level_label_alert' => 'إنذار',
        'app_log_level_label_emergency' => 'حالة طوارئ',
        'app_url_label' => 'عنوان URL للتطبيق',
        'app_url_placeholder' => 'عنوان URL للتطبيق',
        'db_connection_failed' => 'لا يمكن الاتصال بقاعدة البيانات.',
        'db_connection_label' => 'اتصال قاعدة البيانات',
        'db_connection_label_mysql' => 'mysql',
        'db_connection_label_sqlite' => 'سكلايت',
        'db_connection_label_pgsql' => 'pgsql',
        'db_connection_label_sqlsrv' => 'sqlsrv',
        'db_host_label' => 'مضيف قاعدة البيانات',
        'db_host_placeholder' => 'مضيف قاعدة البيانات',
        'db_port_label' => 'منفذ قاعدة البيانات',
        'db_port_placeholder' => 'منفذ قاعدة البيانات',
        'db_name_label' => 'اسم قاعدة البيانات',
        'db_name_placeholder' => 'اسم قاعدة البيانات',
        'db_username_label' => 'اسم مستخدم قاعدة البيانات',
        'db_username_placeholder' => 'اسم مستخدم قاعدة البيانات',
        'db_password_label' => 'كلمة مرور قاعدة البيانات',
        'db_password_placeholder' => 'كلمة مرور قاعدة البيانات',
        'app_tabs' => 
        array (
          'more_info' => 'مزيد من المعلومات',
        ),
      ),
    ),
  ),
);
