<?php
/**
 * File name: OrderController.php
 * Last modified: 2020.04.29 at 18:37:35
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers;

use App\Criteria\Orders\OrdersOfUserCriteria;
use App\Criteria\Users\ClientsCriteria;
use App\Criteria\Users\DriversCriteria;
use App\Criteria\Users\DriversOfStoreCriteria;
use App\DataTables\OrderDataTable;
use App\DataTables\ProductOrderDataTable;
use App\Events\OrderChangedEvent;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Notifications\AssignedOrder;
use App\Notifications\StatusChangedOrder;
use App\Repositories\CustomFieldRepository;
use App\Repositories\NotificationRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderStatusRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class  OrderController extends Controller
{
    /** @var  OrderRepository */
    private $orderRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var OrderStatusRepository
     */
    private $orderStatusRepository;
    /** @var  NotificationRepository */
    private $notificationRepository;
    /** @var  PaymentRepository */
    private $paymentRepository;

    public function __construct(OrderRepository $orderRepo, CustomFieldRepository $customFieldRepo, UserRepository $userRepo
        , OrderStatusRepository $orderStatusRepo, NotificationRepository $notificationRepo, PaymentRepository $paymentRepo)
    {
        parent::__construct();
        $this->orderRepository = $orderRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->userRepository = $userRepo;
        $this->orderStatusRepository = $orderStatusRepo;
        $this->notificationRepository = $notificationRepo;
        $this->paymentRepository = $paymentRepo;
    }

    /**
     * Display a listing of the Order.
     *
     * @param OrderDataTable $orderDataTable
     * @return Response
     */
    public function index(OrderDataTable $orderDataTable)
    {
        return $orderDataTable->render('orders.index');
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        $user = $this->userRepository->getByCriteria(new ClientsCriteria())->pluck('name', 'id');
        $driver = $this->userRepository->getByCriteria(new DriversCriteria())->pluck('name', 'id');

        $orderStatus = $this->orderStatusRepository->pluck('status', 'id');

        $hasCustomField = in_array($this->orderRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->orderRepository->model());
            $html = generateCustomField($customFields);
        }
        return view('orders.create')->with("customFields", isset($html) ? $html : false)->with("user", $user)->with("driver", $driver)->with("orderStatus", $orderStatus);
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->orderRepository->model());
        try {
            $order = $this->orderRepository->create($input);
            $order->customFieldsValues()->createMany(getCustomFieldsValues($customFields, $request));

        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully', ['operator' => __('lang.order')]));

        return redirect(route('orders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param int $id
     * @param ProductOrderDataTable $productOrderDataTable
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */

    public function show(ProductOrderDataTable $productOrderDataTable, $id)
    {
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        $order = $this->orderRepository->findWithoutFail($id);
        if (empty($order)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.order')]));

            return redirect(route('orders.index'));
        }
        $subtotal = 0;

        foreach ($order->productOrders as $productOrder) {
            $subtotal += $productOrder->price * $productOrder->quantity;
        }

        $total = $subtotal + $order['delivery_fee'];
        $total += ($total * $order['tax'] / 100);
        $productOrderDataTable->id = $id;

        return $productOrderDataTable->render('orders.show', ["order" => $order, "total" => $total, "subtotal" => $subtotal]);
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function edit($id)
    {
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        $order = $this->orderRepository->findWithoutFail($id);
        if (empty($order)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.order')]));

            return redirect(route('orders.index'));
        }

        $store = $order->productOrders()->first();
        $store = isset($store) ? $store->product['store_id'] : 0;

        $user = $this->userRepository->getByCriteria(new ClientsCriteria())->pluck('name', 'id');
        $driver = $this->userRepository->getByCriteria(new DriversOfStoreCriteria($store))->pluck('name', 'id');
        $orderStatus = $this->orderStatusRepository->pluck('status', 'id');


        $customFieldsValues = $order->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->orderRepository->model());
        $hasCustomField = in_array($this->orderRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('orders.edit')->with('order', $order)->with("customFields", isset($html) ? $html : false)->with("user", $user)->with("driver", $driver)->with("orderStatus", $orderStatus);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update($id, UpdateOrderRequest $request)
    {
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        $oldOrder = $this->orderRepository->findWithoutFail($id);
        if (empty($oldOrder)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.order')]));
            return redirect(route('orders.index'));
        }
        $oldStatus = $oldOrder->payment->status;
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->orderRepository->model());
        try {
//            if (isset($input['driver_id']) && $input['driver_id'] == "0") {
//                unset($input['driver_id']);
//            }
            $order = $this->orderRepository->update($input, $id);
            $getOrderDetails=\DB::table('orders')->where('id',$id)->first();

            $order->load(['user', 'payment', 'deliveryAddress', 'productOrders', 'products']);
            // ship To Aramex
            if ($getOrderDetails->shipping_method === 'armex' && $getOrderDetails->order_status_id === 4 && $oldOrder->order_status_id <= 3){
                $orderId=$order->id;
                $line1=$order->deliveryAddress->address;
                $clientName=$order->user->name;
                $clientPhoneNumber=$order->user->custom_fields['phone']['value'];
                $clientEmail=$order->user->email;
                $description=$order->deliveryAddress->description;
                $weight=$order->products->sum('capacity');
                $cost=$order->payment->price;
                $pieces=$order->productOrders->sum('quantity');
                $response = $this->shipToAramex($orderId,$line1,$clientName,$clientPhoneNumber,$clientEmail,$description,$weight,$cost,$pieces);

                if ($response['hasErrors']) {
                    $order->order_status_id = $oldOrder->order_status_id;
                    $order->save();
                    echo 'الرجاء التأكد من وجود المعلومات كاملة مثل اسم الحي و اسم الشارع';
                }
                $order->shipment_policy = $response['shipment_policy'];
                $order->shipment_label_url = $response['labelURL'];
                $order->save();
            }
            if (setting('enable_notifications', false)) {
                if ($input['order_status_id'] != $oldOrder->order_status_id) {
                    Notification::send([$order->user], new StatusChangedOrder($order));
                }

                if (isset($input['driver_id']) && ($input['driver_id'] != $oldOrder['driver_id'])) {
                    $driver = $this->userRepository->findWithoutFail($input['driver_id']);
                    if (!empty($driver)) {
                        Notification::send([$driver], new AssignedOrder($order));
                    }
                }
            }

            $this->paymentRepository->update([
                "status" => $input['status'],
            ], $order['payment_id']);

            event(new OrderChangedEvent($order,$oldStatus));

            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $order->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully', ['operator' => __('lang.order')]));

        return redirect(route('orders.index'));
    }

    /**
     * Remove the specified Order from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function destroy($id)
    {
        if (!env('APP_DEMO', false)) {
            $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
            $order = $this->orderRepository->findWithoutFail($id);

            if (empty($order)) {
                Flash::error(__('lang.not_found', ['operator' => __('lang.order')]));

                return redirect(route('orders.index'));
            }

            $this->orderRepository->delete($id);

            Flash::success(__('lang.deleted_successfully', ['operator' => __('lang.order')]));


        } else {
            Flash::warning('This is only demo app you can\'t change this section ');
        }
        return redirect(route('orders.index'));
    }

    /**
     * Remove Media of Order
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $order = $this->orderRepository->findWithoutFail($input['id']);
        try {
            if ($order->hasMedia($input['collection'])) {
                $order->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
    
    public function shipToAramex(
        $orderId,
        $line1,
        $clientName,
        $clientPhoneNumber,
        $clientEmail,
        $description,
        $weight,
        $cost,
        $pieces
    ) {
        $paymentMethod='c';
        $soapClient = new \SoapClient(asset('shipping-services-api-wsdl.wsdl'));
        $params = [
            'Shipments' => [
                'Shipment' => [
                    'Shipper' => [
                        'Reference1'    => 'Alammari',
                        'AccountNumber' => '60497291',
                        'PartyAddress'  => [
                            'Line1'               => 'Al Makaronah',
                            'Line2'               => 'Mishrifah ',
                            'Line3'               => 'Al Sharq Shopping Ctr. Mushrefah',
                            'City'                => 'Jeddah',
                            'StateOrProvinceCode' => 'JED',
                            'PostCode'            => '23336',
                            'CountryCode'         => 'SA',
                        ],
                        'Contact'       => [
                            'Department'   => 'sales',
                            'PersonName'   => 'Al Sharq Shopping Ctr. Mushrefah',
                            'Title'        => 'Alammari Textiles',
                            'CompanyName'  => 'Deal of Sell ',
                            'PhoneNumber1' => '966500357567',
                            'CellPhone'    => '966559090879',
                            'EmailAddress' => 'info@softlix.net',
                        ],
                    ],
    
                    'Consignee' => [
                        'Reference1'   => $orderId,
                        'AccountNumber' => '60497291',
                        'PartyAddress' => [
                            'Line1' => $line1,
                            // اسم الحس
                            'Line2' => '',
                            // اسم الشارع
                            'Line3' => '',
                            //  العنوان كامل
    
    
                            'City'                => 'Jeddah',
                            'StateOrProvinceCode' => 'Jeddah',
                            'PostCode'            => '23336',
                            'CountryCode'         => 'SA',
                        ],
    
                        'Contact' => [
                            'PersonName'   => $clientName,
                            'Title'        => 'Mr.',
                            'CompanyName'  => $clientName,
                            'PhoneNumber1' => $clientPhoneNumber,
                            'CellPhone'    => $clientPhoneNumber,
                            'EmailAddress' => $clientEmail,
                        ],
                    ],
    
                    'ShippingDateTime' => date('Y-m-d'),
                    'Comments'         => $description,
                    'Reference1'       => $orderId,
                    'Details'          => [
                        'ActualWeight' => [
                            'Value' => $weight,
                            'Unit'  => 'Kg',
                        ],
    
                        'ProductGroup'   => 'DOM',
                        'ProductType'    => 'ONP',
                        'PaymentType'    => 'c',
                        'PaymentOptions' => 'ARCC',
    
                        'NumberOfPieces'     => $pieces,
                        'DescriptionOfGoods' => $description,
                        'GoodsOriginCountry' => 'SA',
                        // based on services
    
    
                        //  'Items' => [],
                    ],
                ],
            ],
    
            'ClientInfo' => [
                'AccountCountryCode' => 'SA',
                'AccountEntity'      => 'JED',
                'AccountNumber'      => '60497291',
                'AccountPin'         => '115216',
                'UserName'           => 'info@dealofsell.com',
                'Password'           => 'Asd@9090Asd',
                'Version'            => 'v1.0',
            ],
    
            'Transaction' => [
                'Reference1' => $orderId, // order number
    
            ],
            'LabelInfo'   => [
                'ReportID'   => 9201,
                'ReportType' => 'URL',
            ],
        ];
    
        if ($paymentMethod == 'cod') {
            // 'Services'             => 'CODS',
            $params['Shipments']['Shipment']['Details']['Services'] = 'CODS';
            $params['Shipments']['Shipment']['Details']['CashOnDeliveryAmount'] = [
                'Value'        => $cost,
                'CurrencyCode' => 'SAR',
            ];
            $params['Shipments']['Shipment']['Details']['CollectAmount'] = [
                'Value'        => $cost,
                'CurrencyCode' => 'SAR',
            ];
        }
    
        try {
            $hasErrors = false;
            $errors = [];
            $auth_call = $soapClient->CreateShipments($params);
            if ($auth_call->HasErrors) {
                $hasErrors = true;
                $response = $auth_call->Shipments->ProcessedShipment->Notifications;

                if (is_object($response)) {
                    $errors[] = $response->Notification->Message;
                } else {
                    foreach ($response as $notification) {
                        $errors[] = $notification->Message;
                    }
                }
    
                return [
                    'hasErrors' => $hasErrors,
                    'errors'    => $errors,
                ];
    
            } else {
    
                return [
                    'hasErrors'       => $hasErrors,
                    'shipment_policy' => $auth_call->Shipments->ProcessedShipment->ID,
                    'labelURL'        => $auth_call->Shipments->ProcessedShipment->ShipmentLabel->LabelURL,
                ];
    
    
            }
    
        } catch (SoapFault $fault) {
            exit('Error : ' . $fault->faultstring);
        }
    }
    
    public function callAramex(){
        // trigger
        $orderId='1';
        $line1='st , fif, sa';
        $clientName='mohammed';
        $clientPhoneNumber='01000922720';
        $clientEmail='mohammedhefny75@gmail.com';
        $description='test';
        $weight='22';
        $cost='1200';
        $pieces='3';
        $response = $this->shipToAramex($orderId,$line1,$clientName,$clientPhoneNumber,$clientEmail,$description,$weight,$cost,$pieces);
        
        if ($response['hasErrors']) {
            echo 'الرجاء التأكد من وجود المعلومات كاملة مثل اسم الحي و اسم الشارع';
        }
        
        echo '<pre>';
        print_r($response);
        echo '</pre>';
    }
}
